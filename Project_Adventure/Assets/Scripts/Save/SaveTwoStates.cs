﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SaveTwoStates : SaveBasic
{
    
    public UnityEvent saveEvent;
    public UnityEvent didntSaveEvent;
    public string saveID;
    public bool hasSaved = false;


    public void TriggerSave() {
        hasSaved = true;
    }

    public override void SaveObject() {
        if (hasSaved)
            PlayerPrefs.SetInt(saveID, 1);
    }

    public override void LoadObject() {
        if (PlayerPrefs.GetInt(saveID, 0) == 0) { //Nao Salvo
            didntSaveEvent.Invoke();
        }
        else { //Salvo
            saveEvent.Invoke();
        }
    }
}
