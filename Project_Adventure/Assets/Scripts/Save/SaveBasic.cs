﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveBasic : MonoBehaviour
{

    protected virtual void Start() {
        SaveManager.Instance.UpdateSave += SaveObject;
        SaveManager.Instance.LoadSave += LoadObject;
    }

    public virtual void SaveObject() {

    }

    public virtual void LoadObject() {

    }

    protected virtual void OnDestroy() {
        SaveManager.Instance.UpdateSave -= SaveObject;
        SaveManager.Instance.LoadSave -= LoadObject;
    }
}
