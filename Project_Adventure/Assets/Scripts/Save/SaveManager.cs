﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    static SaveManager instance;
    public static SaveManager Instance { get { return instance; } }

    public delegate void SaveObject();
    public SaveObject LoadSave;
    public SaveObject UpdateSave;


    void Awake() {
        instance = this;
        StartCoroutine(LoadSaveWithDelay());
    }

    public void SaveGameNow() {
        if (UpdateSave != null)
            UpdateSave();
    }

    public void LoadGameNow() {
        if (LoadSave != null)
            LoadSave();
    }

    IEnumerator LoadSaveWithDelay() {
        yield return new WaitForSeconds(0.2f);
        LoadGameNow();
    }
}
