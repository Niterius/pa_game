﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetector : MonoBehaviour
{
    public bool isGrounded = false;
    int count = 0;


    void OnTriggerEnter(Collider other) {
        count++;
        isGrounded = true;
    }

    void OnTriggerExit(Collider other) {
        count--;
        if (count <= 0)
            isGrounded = false;
    }
}
