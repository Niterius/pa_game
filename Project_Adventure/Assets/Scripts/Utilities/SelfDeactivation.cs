﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDeactivation : MonoBehaviour
{
    public float selfDeactivationTime = 0.3f;
    float timer = 0;


    void OnEnable()
    {
        timer = 0;
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= selfDeactivationTime)
            gameObject.SetActive(false);
    }
}
