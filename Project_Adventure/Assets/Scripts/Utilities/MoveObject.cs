﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{
    public GameObject target;
    public Transform openPos;
    public Transform closePos;
    public float duration;
    public string easeType = "easeinoutquad";


    public void CloseNow() {
        iTween.MoveTo(target, iTween.Hash("time", duration, "position", closePos.position, "easetype", easeType));
    }

    public void OpenNow() {
        iTween.MoveTo(target, iTween.Hash("time", duration, "position", openPos.position, "easetype", easeType));
    }
}
