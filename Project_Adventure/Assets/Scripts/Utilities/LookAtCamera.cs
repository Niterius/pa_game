﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    
    public Vector3 rotationOffset;

    void Update() {
        transform.LookAt(Camera.main.transform);
        transform.eulerAngles += rotationOffset;
    }
}
