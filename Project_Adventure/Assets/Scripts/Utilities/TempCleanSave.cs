﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempCleanSave : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
            PlayerPrefs.DeleteAll();
    }
}
