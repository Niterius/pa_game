﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HealthBar : MonoBehaviour
{
    public CanvasGroup fadeGroup;
    public Image healthBar;

    void Awake() {
        var being = GetComponent<ComplexLivingBeing>();
        if (being) {
            being.Damaged += UpdateHealthBar;
            being.Damaged += ShowHealthBar;
            being.UpdateHP += UpdateHealthBar;
            being.ImDead += HideHealthBar;
        }

        fadeGroup.alpha = 0;
    }

    public void UpdateHealthBar() {
        var being = GetComponent<ComplexLivingBeing>();
        if (being)
            healthBar.fillAmount = being.GetHPRate(); //0 a 1
    }

    public void ShowHealthBar() {
        if (fadeGroup.alpha < 0.1)
            iTween.ValueTo(gameObject, iTween.Hash("time", 1, "from", fadeGroup.alpha, "to", 1, "onupdate", "SetFadeGroupAlpha"));
    }

    public void HideHealthBar() {
        iTween.ValueTo(gameObject, iTween.Hash("time", 2, "from", fadeGroup.alpha, "to", 0, "onupdate", "SetFadeGroupAlpha"));
    }

    public void SetFadeGroupAlpha (float newAlpha) {
        fadeGroup.alpha = newAlpha;
    }
}
