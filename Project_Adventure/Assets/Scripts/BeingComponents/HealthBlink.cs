﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBlink : MonoBehaviour
{
    public Renderer[] myRenderer;
    public string colorParameter = "_Color";
    public Color blinkColor = Color.red;


    void Start() {
        var being = GetComponent<ComplexLivingBeing>();
        if (being)
            being.Damaged += BlinkOnDamage;
    }

    public void BlinkOnDamage() {
        for (int j=0; j<myRenderer.Length; j++) {
            for (int i=0; i<myRenderer[j].materials.Length; i++) {
                myRenderer[j].materials[i].SetColor(colorParameter, blinkColor);
            }
        }

        StartCoroutine(ChangeColorBack());
    }

    IEnumerator ChangeColorBack() {
        yield return new WaitForSeconds(0.3f);
        for (int j=0; j<myRenderer.Length; j++) {
            for (int i=0; i<myRenderer[j].materials.Length; i++) {
                myRenderer[j].materials[i].SetColor(colorParameter, Color.white);
            }
        }
    }
}
