﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthParticles : MonoBehaviour
{

    public ParticleSystem damageParticles;


    void Awake() {
        var being = GetComponent<ComplexLivingBeing>();
        if (being) {
            being.Damaged += ParticlesOnDamage;
        }
    }

    public void ParticlesOnDamage() {
        damageParticles.Play();
    }
}
