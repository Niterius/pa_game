﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NatureOfDamage { MELEE, RANGED, SPECIAL }
public enum TypeOfDamage { CUT, PIERCE, SMASH, MAGICAL }

public class Weapon : BasicGear
{
    public float damage = 1;
    public float weaponRange = 1;
    public NatureOfDamage damageNature = NatureOfDamage.MELEE;
    public TypeOfDamage damageType = TypeOfDamage.CUT;


    void OnTriggerEnter(Collider col) {
        LivingBeing being = col.GetComponent<LivingBeing>();
        if (being)
        {
            being.TakeDamage(damage);
            TakeDamage(1);
        }
    }

    protected override void Death()
    {
        base.Death();
        Destroy(gameObject);
    }
}
