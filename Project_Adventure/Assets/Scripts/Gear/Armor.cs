﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Armor : BasicGear
{
    public float defense = 1;

    protected override void Death() {
        base.Death();
        Destroy(gameObject);
    }
}
