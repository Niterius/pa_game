﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEventWithKey : MonoBehaviour
{
    public KeyCode keyToPress = KeyCode.E;
    public string tagToCompare = "Player";
    public UnityEvent triggerEvent;


    void OnTriggerStay(Collider other) {
        if (other.CompareTag(tagToCompare)  &&   Input.GetKeyDown(keyToPress))
            triggerEvent.Invoke();
    }
}
