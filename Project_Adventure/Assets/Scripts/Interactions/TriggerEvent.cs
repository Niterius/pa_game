﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEvent : MonoBehaviour
{
    public string acceptableTag = "Player";
    public UnityEvent enterEvents;
    public UnityEvent exitEvents;


    void OnTriggerEnter(Collider other) {
        if (other.CompareTag(acceptableTag))
            enterEvents.Invoke();
    }

    void OnTriggerExit(Collider other) {
        if (other.CompareTag(acceptableTag))
            exitEvents.Invoke();
    }
}
