﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    static TimeManager instance;
    public static TimeManager Instance { get { return instance; } }
    

    void Awake() {
        instance = this;
    }

    public void PauseGame() {
        Time.timeScale = 0;
    }

    public void UnpauseGame() {
        Time.timeScale = 1;
    }
}
