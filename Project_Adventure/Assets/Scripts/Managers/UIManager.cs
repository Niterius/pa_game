﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    static UIManager instance;
    public static UIManager Instance { get { return instance; } }
    public bool gameIsPaused = false;
    public GameObject pausePanel;


    void Awake() {
        instance = this;
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
            PauseProcedures();
    }

    public void SaveGameButton() {
        SaveManager.Instance.SaveGameNow();
        PauseProcedures();
    }

    public void LoadGameButton() {
        SaveManager.Instance.LoadGameNow();
        PauseProcedures();
    }

    void PauseProcedures() {
        gameIsPaused = !gameIsPaused;
        pausePanel.SetActive(gameIsPaused);

        if (gameIsPaused)
            TimeManager.Instance.PauseGame();
        else
            TimeManager.Instance.UnpauseGame();
    }
}
