﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBoolValue : StateMachineBehaviour
{
    public string boolName = "";
    public bool newValue = true;

    public Vector2 delayedTime = new Vector2(0, 1);
    float timer = 0;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = Random.Range(delayedTime.x, delayedTime.y);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer -= Time.deltaTime;
        if (timer <= 0) {
            animator.SetBool(boolName, newValue);
        }
    }
}
