﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRandomInteger : StateMachineBehaviour
{
    public string intName = "ToIdle";
    public int min = 1, max = 4;
    public float timeToSortNewInt = 4;
    float timer = 0;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 0;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer += Time.deltaTime;
        if (timer > timeToSortNewInt) {
            int randomValue = Random.Range(min, max+1);
            animator.SetInteger(intName, randomValue);
            timer = 0;
        }
    }
}
