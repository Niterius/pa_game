﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchForTarget : StateMachineBehaviour
{
    public string target = "Target";
    public string targetDistance = "TargetDistance";
    EnemyWithBehavior enemy;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.GetComponent<EnemyWithBehavior>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(target, enemy.targetDetector.target);
        if (enemy.targetDetector.target)
            animator.SetFloat(targetDistance, Vector3.Distance(animator.transform.position, enemy.targetDetector.target.position));
    }
}
