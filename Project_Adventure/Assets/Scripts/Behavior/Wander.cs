﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Wander : StateMachineBehaviour
{
    EnemyWithBehavior enemy;
    public float wanderSpeed=2;
    float normalSpeed;
    float normalStoppingDistance;
    public float wanderStoppingDistance = 1;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.GetComponent<EnemyWithBehavior>();
        normalSpeed = enemy.agent.speed;
        enemy.agent.speed = wanderSpeed;

        Vector3 targetPosition = animator.transform.position + new Vector3(Random.Range(-5f, 5f), 0, Random.Range(-5, 5));
        enemy.agent.SetDestination(targetPosition);

        normalStoppingDistance = enemy.agent.stoppingDistance;
        enemy.agent.stoppingDistance = wanderStoppingDistance;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Vector3.Distance(animator.transform.position, enemy.agent.destination) <= enemy.agent.stoppingDistance)
            animator.SetBool("Wander", false);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy.agent.speed = normalSpeed;
        enemy.agent.stoppingDistance = normalStoppingDistance;
    }
}
