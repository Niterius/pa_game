﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetIntegerTo : StateMachineBehaviour
{
    public string intName = "ToIdle";
    public int intValue = 0;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetInteger(intName, intValue);
    }
}
