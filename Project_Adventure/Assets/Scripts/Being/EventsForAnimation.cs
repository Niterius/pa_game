﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventsForAnimation : MonoBehaviour
{
    public UnityEvent[] animationEvents;

    public void CallEvent(int index) {
        animationEvents[index].Invoke();
    }
}
