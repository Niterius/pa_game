﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : ComplexLivingBeing {
    
    Rigidbody rgbd;
    public float walkSpeed = 5;
    public float rotationSpeed = 120;
    public Transform stepRaycastRef;
    public LayerMask stepLayerMask;
    public int stepUpForce = 10;
    public GroundDetector groundDetector;


    protected override void Awake() {
        base.Awake();
        rgbd = GetComponent<Rigidbody>();
    }

    void FixedUpdate() {
        //Movimento
        rgbd.velocity += (transform.forward * Input.GetAxis("Vertical") * walkSpeed * Time.fixedDeltaTime);

        //Rotacao
        Vector3 planarVelocity = new Vector3(rgbd.velocity.x, 0, rgbd.velocity.z);
        if (planarVelocity.magnitude > 1)
            transform.Rotate(transform.up * Input.GetAxis("Horizontal") * rotationSpeed * Time.fixedDeltaTime);

        //Step
        Vector3 newVelocity = new Vector3(rgbd.velocity.x, 0, rgbd.velocity.z);
        if (rgbd.velocity.magnitude > 0.1f) {
            RaycastHit hit;
            if (Physics.Raycast(stepRaycastRef.position, Vector3.down, out hit, 0.6f, stepLayerMask)) {
                rgbd.AddForce(Vector3.up * stepUpForce * Time.fixedDeltaTime);
            }
        }

        //Gravity
        if (!groundDetector.isGrounded) {
            rgbd.velocity += Physics.gravity * Time.fixedDeltaTime * 10; 
        }
    }
}
