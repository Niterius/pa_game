﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingBeing : MonoBehaviour
{
    
    public int maxHitPoints = 5;
    protected float currentHitPoints;
    public bool isDead { get { return (currentHitPoints <= 0); } }


    protected virtual void Awake() {
        currentHitPoints = maxHitPoints;
    }

    public virtual bool TakeDamage(float damage) {
        if (isDead)
            return false;

        currentHitPoints -= damage;
        if (isDead)
            Death();

        return true;
    }

    protected virtual void Death() {
        currentHitPoints = 0;
    }

    public virtual void Healing(float healAmount) {
        currentHitPoints = Mathf.Min(currentHitPoints + healAmount, maxHitPoints);
    }
}
