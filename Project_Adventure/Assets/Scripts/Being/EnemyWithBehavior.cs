﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyWithBehavior : ComplexLivingBeing
{
    public TargetDetector targetDetector;
    public NavMeshAgent agent;
    Animator animator;


    protected override void Awake()
    {
        base.Awake();
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    protected override void Death() {
        base.Death();
        animator.SetBool("Death", true);
    }

    public override void Healing(float healAmount) {
        base.Healing(healAmount);
    }
}