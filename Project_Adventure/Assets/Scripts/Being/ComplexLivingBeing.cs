﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComplexLivingBeing : LivingBeing {

    public int naturalDefense = 0;
    public Armor armor;
    public Weapon weapon;

    public delegate void ChangeLife();
    public ChangeLife Damaged;
    public ChangeLife UpdateHP;
    public ChangeLife ImDead;


    protected override void Awake() {
        base.Awake();

        if (UpdateHP != null)
            UpdateHP();
    }

    public override bool TakeDamage(float damage) {
        if (armor)
        {
            armor.TakeDamage(damage);
            damage -= armor.defense;
            if (damage <= 0)
                return false;
        }

        damage -= naturalDefense;
        if (damage <= 0) 
            return false;
        
        if (Damaged != null)
            Damaged();

        return base.TakeDamage(damage);
    }

    public float GetHPRate() {
        return currentHitPoints/(float)maxHitPoints;
    }

    public override void Healing(float healAmount) {
        base.Healing(healAmount);

        if (UpdateHP != null)
            UpdateHP();
    }

    protected override void Death() {
        base.Death();

        if (UpdateHP != null)
            UpdateHP();

        if (ImDead != null)
            ImDead();
    }
}
