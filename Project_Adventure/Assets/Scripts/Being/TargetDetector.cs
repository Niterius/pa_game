﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetDetector : MonoBehaviour
{
    public Transform target;

    void OnTriggerEnter(Collider col) {
        if (target) {
            float newDistance = Vector3.Distance(col.transform.position, transform.position);
            float oldDistance = Vector3.Distance(target.transform.position, transform.position);
            if (newDistance >= oldDistance)
                return;
        }

        LivingBeing being = col.GetComponent<LivingBeing>();
        if (being)
            target = being.transform;

    }

    void OnTriggerExit(Collider col) {
        if (!target)
            return;

        if (col.gameObject == target.gameObject)
            target = null;
    }
}
