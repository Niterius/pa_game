﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public enum EnemyStates { IDLE, RUN, PATROL, ATTACK, DEAD }


public class Enemy : ComplexLivingBeing {

    protected EnemyStates currentState = EnemyStates.IDLE;
    public TargetDetector targetDetector;
    NavMeshAgent agent;
    float timer = 0;
    public List<Weapon> weaponList = new List<Weapon>();


    protected override void Awake() {
        base.Awake();

        for (int i=0; i<weaponList.Count; i++)
            weaponList[i].gameObject.SetActive(false);

        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        switch (currentState)
        {
            case EnemyStates.IDLE:
                UpdateIdle();
                break;
            case EnemyStates.RUN:
                UpdateRun();
                break;
            case EnemyStates.PATROL:
                UpdatePatrol();
                break;
            case EnemyStates.ATTACK:
                UpdateAttack();
                break;
            case EnemyStates.DEAD:
                UpdateDead();
                break;
        }
    }

    void UpdateIdle() {
        if (targetDetector.target) {
            currentState = EnemyStates.RUN;
            ChangeWeapon();
        }

        timer += Time.deltaTime;
        if (timer > 2) {
            timer = 0;
            StartPatrol();
        }
    }

    void StartPatrol() {
        Vector3 targetPosition = transform.position +
            new Vector3(Random.Range(-5f, 5f), 0, Random.Range(-5, 5));

        agent.SetDestination(targetPosition);
        currentState = EnemyStates.PATROL;
        agent.stoppingDistance = 1;
    }

    void UpdateRun() {
        //Verificar se tem um target, se não tiver, voltar a ficar parado
        if (!targetDetector.target) {
            currentState = EnemyStates.IDLE;
            return;
        }

        //Se tem um target, move
        agent.SetDestination(targetDetector.target.position);


        //Ataque
        if (!weapon)
            return;

        if (Vector3.Distance(transform.position, targetDetector.target.position) 
            <= weapon.weaponRange)
        {
            currentState = EnemyStates.ATTACK;
            timer = 0;
        }
    }

    void UpdateAttack()
    {
        if (!weapon)
        {
            currentState = EnemyStates.IDLE;
            return;
        }

        timer += Time.deltaTime;
        if (timer > 2)
        {
            weapon.gameObject.SetActive(true);
            timer = 0;
        }

        if (Vector3.Distance(transform.position, targetDetector.target.position)
            > weapon.weaponRange) {
            currentState = EnemyStates.RUN;
        }
    }

    void UpdatePatrol() {
        if (targetDetector.target) {
            currentState = EnemyStates.RUN;
            ChangeWeapon();
            return;
        }

        if (Vector3.Distance(transform.position, agent.destination) <= 
            agent.stoppingDistance) {
            currentState = EnemyStates.IDLE;
        }
    }

    void UpdateDead() {

    }

    /// <summary>
    /// Este metodo troca para uma arma aleatoria da lista de armas weaponList
    /// </summary>
    public void ChangeWeapon() {
        if (weaponList.Count == 0)
            return;

        int randomValue = Random.Range(0, weaponList.Count);
        weapon = weaponList[randomValue];
        agent.stoppingDistance = weapon.weaponRange;
    }

    /// <summary>
    /// Este metodo troca para uma arma especifica, de acordo com o weaponIndex passado
    /// </summary>
    /// <param name="weaponIndex"> este parametro e o index da lista de armas </param>
    public void ChangeWeapon(int weaponIndex) {
        if (weaponIndex >= weaponList.Count)
            return;

        weapon = weaponList[weaponIndex];
        agent.stoppingDistance = weapon.weaponRange;
    }
}
